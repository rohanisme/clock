import React from "react";
import Trackerlayout from "./timelayout";
import TimerLap from "./timerLap";

const Timetracker = () => {
  return (
    <div>
      <Trackerlayout />
      <TimerLap />
    </div>
  );
};

export default Timetracker;
