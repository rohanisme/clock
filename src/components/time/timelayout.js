import React from "react";

const Trackerlayout = () => {
  return (
    <div className="section-tracker">
      <div className="row">
        <div className="timebox">
          <div className="timebox__form">
            <form action="/" className="form">
              <div className="u-margin-bottom-medium"></div>

              <div className="form__group">
                <input
                  type="text"
                  className="form__input"
                  placeholder="What have you worked on?"
                  id="name"
                  required
                />

                <a href="/" className="btn btn--transparent btn--animated">
                  <img
                    src={require("../../img/icons/plus-blue.svg")}
                    height="20px"
                    alt="plus button icon"
                    witdh="20px"
                  />
                  Project
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Trackerlayout;
