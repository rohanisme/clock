import React from "react";
import Timetracker from "./components/time";

function App() {
  return (
    <div>
      <Timetracker />
    </div>
  );
}

export default App;
